package com.wwmxd.service;

import com.wwmxd.entity.RoleMenu;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author WWMXD
 * @since 2018-01-03 14:39:38
 */
public interface RoleMenuService extends  IService<RoleMenu> {
	
}
