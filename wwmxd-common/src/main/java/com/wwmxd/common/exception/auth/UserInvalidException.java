package com.wwmxd.common.exception.auth;


import com.wwmxd.common.constant.CommonConstants;
import com.wwmxd.common.exception.BaseException;

/**
 * Created by ace on 2017/9/10.
 */
public class UserInvalidException extends BaseException {
    public UserInvalidException(String message) {
        super(message, CommonConstants.EX_USER_INVALID_CODE);
    }
}
